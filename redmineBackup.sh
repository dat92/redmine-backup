#!/bin/bash

#Version 1.1

#Config section
#Log file
#Don't forget to add file rotation
LOG_FILE=/var/log/redmineBackup.log

#Path

#Local store
#Prefix for acrchives
PREFIX="RedmineBackup"
#Local store for backups
LOCAL_BACKUP_FOLDER="/home/dat/RedmineBackup/"

#Containers
#Redmine container
#Redmine container name
REDMINE_CONTAINER_NAME=redmine

#Path to container's volume with "redmine" configuration
CONTAINER_VOLUME_PATH="/var/lib/docker/volumes/redmine-test_redmine-data/_data/"

#DB container
#DB Container name
DB_CONTAINER_NAME=""

#DB config
DB_HOST=localhost
DB_NAME=bitnami_redmine
DB_USER=mysql
DB_PASSWD=password

#Rotate configuration
MAX_ARCHIVES_COPIES=3

#End configuration section

printhelp() {
	echo "Usage: redmineBackup.sh [keys...]"
	echo "Keys:"
	echo "  -b, --full-backup                                             make full backup"
	echo "  -d, --folder=FULL_PATH_FOR_BACKUPS                            override full path for backup"
	echo "  -l, --log=LOG_NAME                                            override log's name"
	echo "  -v, --redmine-volume=FULL_PATH_TO_REDMINE_CONTAINER'S_VOLUME  override full path to Redmine container's volume"
    echo "  -h, --dbhost=HOST_OF_DB                                       override db hostname"
    echo "  -n, --dbname=DB_NAME                                          override db name"
    echo "  -u, --dbuser=DB_USER                                          override db user"
    echo "  -p, --dbpassword=DB_PASSWORD                                  override (or specific) db password"
	
}

if [ "$1" == "--help" ]; then
	printhelp;
	exit 0;
fi

declare -i FULL_BACKUP

for arg in "$@"; do
	shift
	case "$arg" in
		"--full-backup") set -- "$@" "-b" ;;
		"--folder") set -- "$@" "-d" ;;
		"--log") set -- "$@" "-l" ;;
		"--redmine-volume") set -- "$@" "-v" ;;
        "--dbhost") set -- "$@" "-h" ;;
		"--dbname") set -- "$@" "-n" ;;
		"--dbuser") set -- "$@" "-u" ;;
		"--dbpassword") set -- "$@" "-p" ;;
		*) set -- "$@" "$arg"

	esac
done

while getopts "bd:l:v:h:n:u:p:" opt
do
	case $opt in
		b) FULL_BACKUP=1;;
		d) LOCAL_BACKUP_FOLDER=$OPTARG;;
		l) LOG_FILE=$OPTARG;;		
		v) CONTAINER_VOLUME_PATH=$OPTARG;;
        h) DB_HOST=$OPTARG;;
		n) DB_NAME=$OPTARG;;
		u) DB_USER=$OPTARG;;
		p) DB_PASSWD=$OPTARG;;		
	esac
done

START_DATE=`date +%Y.%m.%d.%H%M%S`

if [ "$1" == "$FULL_PARAMETER" ]; then
	PREFIX=$PREFIX.FULL
fi

echo "" >> $LOG_FILE
echo "$START_DATE Start redmine backuping" >> $LOG_FILE

TMP_FOLDER=$LOCAL_BACKUP_FOLDER$PREFIX-$START_DATE/

cleantmp() {
	echo "`date +%Y.%m.%d.%H%M%S` Remove tmp folder $TMP_FOLDER" >> $LOG_FILE
	rm -rf $TMP_FOLDER
	RET_CODE=$?
	echo "`date +%Y.%m.%d.%H%M%S` Return code = $RET_CODE" >> $LOG_FILE
	if (( $RET_CODE != 0 )); then
		echo "`date +%Y.%m.%d.%H%M%S` temporary folder was not deleted" >> $LOG_FILE
	fi
	echo "`date +%Y.%m.%d.%H%M%S` Procedure finished with exit code $1" >> $LOG_FILE
	if [ "$1" != "0" ]; then
		exit $1
	fi
}


echo "`date +%Y.%m.%d.%H%M%S` Create local folder $TMP_FOLDER" >> $LOG_FILE
mkdir -p $TMP_FOLDER 2>> $LOG_FILE
RET_CODE=$?
echo "`date +%Y.%m.%d.%H%M%S` Return code = $RET_CODE" >> $LOG_FILE
if (( $RET_CODE != 0 )); then
	cleantmp $RET_CODE
fi

echo "`date +%Y.%m.%d.%H%M%S` Backup database execution" >>  $LOG_FILE

SQL_BACKUP_CMD="mysqldump -h $DB_HOST -u $DB_USER --password=$DB_PASSWD --databases $DB_NAME --add-drop-database"

if [ -z $DB_CONTAINER_NAME ]; then 
	$SQL_BACKUP_CMD > $TMP_FOLDER$DB_NAME_$START_DATE.sql 2>> $LOG_FILE
else
	docker exec -u 0 $DB_CONTAINER_NAME $SQL_BACKUP_CMD > $TMP_FOLDER$DB_NAME_$START_DATE.sql 2>> $LOG_FILE
fi

RET_CODE=$?
echo "`date +%Y.%m.%d.%H%M%S` Return code = $RET_CODE" >> $LOG_FILE
if (( $RET_CODE != 0 )); then
	cleantmp $RET_CODE
fi

if [ "$1" == "$FULL_PARAMETER" ]; then
	echo "`date +%Y.%m.%d.%H%M%S` Backup full redmine folder" >> $LOG_FILE
	docker exec $REDMINE_CONTAINER_NAME tar -cpf /tmp/redmine-full.$START_DATE.tar -C /opt/bitnami . 2>> $LOG_FILE
	if (( $RET_CODE != 0 )); then
		cleantmp $RET_CODE
	fi
	
	docker cp $REDMINE_CONTAINER_NAME:/tmp/redmine-full.$START_DATE.tar $TMP_FOLDER/ 2>> $LOG_FILE
	if (( $RET_CODE != 0 )); then
		cleantmp $RET_CODE
	fi	
	
	docker exec $REDMINE_CONTAINER_NAME rm /tmp/redmine-full.$START_DATE.tar 2>> $LOG_FILE
	if (( $RET_CODE != 0 )); then
		cleantmp $RET_CODE
	fi
else
	echo "`date +%Y.%m.%d.%H%M%S` Backup redmine folder" >> $LOG_FILE
	tar -cpf $TMP_FOLDER/redmine-data.$START_DATE.tar -C $CONTAINER_VOLUME_PATH . 2>> $LOG_FILE
fi

RET_CODE=$?
echo "`date +%Y.%m.%d.%H%M%S` Return code = $RET_CODE" >> $LOG_FILE
if (( $RET_CODE != 0 )); then
	cleantmp $RET_CODE
fi

echo "`date +%Y.%m.%d.%H%M%S` Backup results" >> $LOG_FILE
tar -czf $LOCAL_BACKUP_FOLDER/$PREFIX.$START_DATE.tar.gz -C $TMP_FOLDER . 2>> $LOG_FILE
RET_CODE=$?
echo "`date +%Y.%m.%d.%H%M%S` Return code = $RET_CODE" >> $LOG_FILE

cleantmp $RET_CODE

echo "`date +%Y.%m.%d.%H%M%S` Check archive's count" >> $LOG_FILE
COPIES_COUNT=`ls $LOCAL_BACKUP_FOLDER | grep $PREFIX | wc -l`
if [ $COPIES_COUNT -gt $MAX_ARCHIVES_COPIES ]; then
	echo "`date +%Y.%m.%d.%H%M%S` Archives = $COPIES_COUNT - needed rotate" >> $LOG_FILE
	NEED_TO_DELETE_ARCHIVES=$(($(ls $LOCAL_BACKUP_FOLDER | wc -l)-$MAX_ARCHIVES_COPIES))

	echo "`date +%Y.%m.%d.%H%M%S` Need to delete $NEED_TO_DELETE_ARCHIVES archives" >> $LOG_FILE
	for (( i=0; i<$NEED_TO_DELETE_ARCHIVES; i++ )); do
		echo "`date +%Y.%m.%d.%H%M%S` Remove `ls $LOCAL_BACKUP_FOLDER | sort | head -n 1` archive" >> $LOG_FILE
		rm $LOCAL_BACKUP_FOLDER/$(ls $LOCAL_BACKUP_FOLDER | sort | head -n 1) 2>> $LOG_FILE 
	done
else
	echo "`date +%Y.%m.%d.%H%M%S` Archives=$COPIES_COUNT - not needed rotate" >> $LOG_FILE
fi

