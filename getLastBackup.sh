#!/bin/bash
#ver 1.0

#Configuration
REMOTE_HOST="192.168.5.183"
REMOTE_USER="dat"
REMOTE_BACKUPS_PATH="/var/redmineBackups/"
LOCAL_BACKUPS_FOLDER="/home/dat/0_DATA/Work/Backups/Redmine/"

#END configuration

printhelp() {
	echo "Copying remote last backup file to local"
	echo "Usage: getLastBackup.sh [keys...]"
	echo "Keys:"
	echo "  -r, --remote=FULL_PATH/     override remote backup folder (default /dev/null)"	
	echo "  -l, --local=FULL_PATH/      override local backup folder (must end with a \"/\")"
}

if [ "$1" == "--help" ]; then
	printhelp;
	exit 0;
fi

for arg in "$@"; do
	shift
	case "$arg" in
		"--remote") set -- "$@" "-r" ;;
		"--local") set -- "$@" "-l" ;;
		*) set -- "$@" "$arg"

	esac
done

while getopts "r:l:" opt
do
	case $opt in
		r) REMOTE_BACKUPS_PATH=$OPTARG;;
		l) LOCAL_BACKUPS_FOLDER=$OPTARG;;		
	esac
done


LAST_BACKUP=`ssh $REMOTE_USER@$REMOTE_HOST ls -t $REMOTE_BACKUPS_PATH | grep -m1 '[0-9]'`
if test -f $LOCAL_BACKUPS_FOLDER$LAST_BACKUP; then
	exit 0;
fi
scp $REMOTE_HOST:"$REMOTE_BACKUPS_PATH$LAST_BACKUP" "$LOCAL_BACKUPS_FOLDER"

